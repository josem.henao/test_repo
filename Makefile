help:
	@echo "build - package the jobs"
	@echo "clean - remove all build and Python file artifacts"
	@echo "clean-build - remove build artifacts"
	@echo "clean-pyc - remove Python file artifacts"

clean: clean-build clean-pyc

clean-build:
	rm -fr dist/
	echo "remove all build files"

clean-libs:
	echo "remove all lib files"
	rm -fr libs/

clean-pyc:
	echo "remove all pyc files"
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-venv:
	pipenv uninstall --all

venv:
	pipenv install

install: clean-libs venv
	pipenv lock -r > requirements.txt
	pipenv run pip install -r requirements.txt -t ./libs

install-local: clean-venv
	pipenv install --dev

lint:
	pipenv run prospector

unit:
	pipenv run python -m unittest discover

deploy-qa:
	echo "deploying to environment qa - push the files to a bucket"

deploy-dev:
	echo "deploying to environment dev - push the files to a bucket"

deploy-staging:
	echo "deploying to environment staging - push the files to a bucket"

deploy-ds:
	echo "deploying to environment ds - push the files to a bucket"

deploy-prod:
	echo "deploying to environment prod - push the files to a bucket"

build: clean make_dist
	cp ./main.py ./dist
	zip -x ./main.py -r ./dist/jobs.zip ./job

make_dist:
	mkdir ./dist